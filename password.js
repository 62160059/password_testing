const checkLength = (password) => {
  return password.length >=  8 && password.length <=25
}

const checkAlphabet = function (password) {
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for(const ch of password){
  //   if(alphabets.includes(ch.toLowerCase())) return true
  // }
  return /[a-zA-Z]/.test(password)
}

const checkDigit = function (password) {
  const Digits = '123456789'
  for (const dg of password) {
    if (Digits.includes(dg)) return password.length >= 8 && password.length <= 25
  }
  return false
}


const checkSymbol = function (password) {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for(const ch of password){
    if(symbols.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)


}

module.exports = {
  checkAlphabet,
  checkLength,
  checkDigit,
  checkSymbol,
  checkPassword
}
